# Valkey

Unofficial Valkey image, so I can already use it in my projects. :)

It was a drop-in replacement for redis for me in my k8s container spec but YMMV.

```sh
docker pull codingpaws/valkey:latest
# or
docker pull registry.gitlab.com/codingpaws/docker/valkey/image:latest
```

---

Valkey source: https://github.com/valkey-io/valkey  
Dockerfile source: https://gitlab.com/codingpaws/docker/valkey
